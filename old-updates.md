### 1.0.3 → 1.0.4
- use PolyMC's automatic update option (choose `[CurseForge]` and `[Confirm for all]`)
- manually remove `bspkrsCore`, `I know what I'm doing`, `MicdoodleCore` and `Inventory Tweaks`.
- download the new versions of [`bspkrsCore`](https://mediafiles.forgecdn.net/files/2924/423/%5b1.12.2%5dbspkrscore-universal-8.0.1.jar), [`MicdoodleCore`](https://micdoodle8.com/new-builds/GC-1.12/280/MicdoodleCore-1.12.2-4.0.2.280.jar) and [`Inventory Tweaks`](https://mediafiles.forgecdn.net/files/2923/460/InventoryTweaks-1.64%2Bdev.151.jar) or copy
```shell
wget https://mediafiles.forgecdn.net/files/2924/423/%5b1.12.2%5dbspkrscore-universal-8.0.1.jar https://micdoodle8.com/new-builds/GC-1.12/280/MicdoodleCore-1.12.2-4.0.2.280.jar https://mediafiles.forgecdn.net/files/2923/460/InventoryTweaks-1.64%2Bdev.151.jar
```
in your terminal and install them to your instance
