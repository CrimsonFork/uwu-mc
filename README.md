# ZXMail modded 1.12.2 MC server

## Download a modpack

- Main download: grab the modpack from [releases](https://gitlab.com/CrimsonFork/uwu-mc/-/releases)
- [alternative download](https://gitea.retro-hax.net/Retro-Hax/uwu-mc)

## """Compiling"""

- use [packwiz](https://packwiz.infra.link) to export the data as a curseforge modpack (modrinth format WIP

## Installation

- make sure you have Java 8
- press `[Add Instance]`
- press `[Import from zip]`
- if launching errors out, make sure the path to Java 8 is correct

## Updating

### Usual
- unless specified otherwise, just use PolyMC's mod update option

### [Older, version specific updates](old-updates.md)

## What's in the modpack?

### minimum requirements:

| mod | slug | version |
| --- | --- | --- |
| [AutoRegLib](https://github.com/VazkiiMods/AutoRegLib) | [autoreglib](https://curseforge.com/minecraft/mc-mods/autoreglib) | [1.3-32](https://maven.blamejared.com/vazkii/autoreglib/AutoRegLib/1.3-32.33/AutoRegLib-1.3-32.33.jar) |
| [Backpacked](https://github.com/MrCrayfish/Backpacked) | [backpacked](https://curseforge.com/minecraft/mc-mods/backpacked) | [1.4.3](https://mediafiles.forgecdn.net/files/3252/551/backpacked-1.4.3-1.12.2.jar) |
| [Beta Days](https://gitlab.com/modding-legacy/beta-days) | [beta-days](https://curseforge.com/minecraft/mc-mods/beta-days) | [v1.2.2](https://mediafiles.forgecdn.net/files/2827/460/beta_days-1.12.2-1.2.2.jar) |
| [bspkrsCore](https://github.com/bspkrs-mods/bspkrsCore) | [bspkrscore](https://curseforge.com/minecraft/mc-mods/bspkrscore) | [8.0.1](https://mediafiles.forgecdn.net/files/2924/423/%5b1.12.2%5dbspkrscore-universal-8.0.1.jar) |
| [Buildcraft](https://mod-buildcraft.com) | [buildcraft](https://curseforge.com/minecraft/mc-mods/buildcraft) | [7.99.24.8](https://mediafiles.forgecdn.net/files/3204/475/buildcraft-all-7.99.24.8.jar) |
| CB Multipart | [cb-multipart](https://curseforge.com/minecraft/mc-mods/cb-multipart) | [2.6.2.83](https://mediafiles.forgecdn.net/files/2755/790/ForgeMultipart-1.12.2-2.6.2.83-universal.jar) |
| [CC: Tweaked](https://tweaked.cc) | [cc-tweaked](https://curseforge.com/minecraft/mc-mods/cc-tweaked) | [1.89.2](https://mediafiles.forgecdn.net/files/2992/872/cc-tweaked-1.12.2-1.89.2.jar) |
| [CodeChicken Lib 1.8.+](https://github.com/TheCBProject/CodeChickenLib) | [codechicken-lib-1-8](https://curseforge.com/minecraft/mc-mods/codechicken-lib-1-8) | [3.2.3.358](https://mediafiles.forgecdn.net/files/2779/848/CodeChickenLib-1.12.2-3.2.3.358-universal.jar) |
| [Computronics](https://github.com/Vexatos/Computronics) | cc-addons | [1.6.6](https://files.vexatos.com/Computronics/Computronics-1.12.2-1.6.6.jar) |
| [Cooking for Blockheads](https://curseforge.com/minecraft/mc-mods/cooking-for-blockheads) | [cooking-for-blockheads](https://curseforge.com/minecraft/mc-mods/cooking-for-blockheads) | [6.5.0](https://mediafiles.forgecdn.net/files/2862/651/CookingForBlockheads_1.12.2-6.5.0.jar) |
| [Cosmettic Armor Reworked](https://github.com/zlainsama/CosmeticArmorReworked) | [cosmetic-armor-reworked](https://curseforge.com/minecraft/mc-mods/cosmetic-armor-reworked) | [1.12.2-v5a]( https://github.com/zlainsama/CosmeticArmorReworked/releases/download/1.12.2-v5a/CosmeticArmorReworked-1.12.2-v5a.jar) |
| [Elevator Mod](https://github.com/VsnGamer/ElevatorMod) | [openblocks-elevator](https://curseforge.com/minecraft/mc-mods/openblocks-elevator) | [1.3.14](https://mediafiles.forgecdn.net/files/2845/365/ElevatorMod-1.12.2-1.3.14.jar) |
| [Familiar Fauna](https://github.com/Glitchfiend/FamiliarFauna) | [familiar-fauna](https://curseforge.com/minecraft/mc-mods/familiar-fauna) | [1.0.11](https://mediafiles.forgecdn.net/files/2644/656/FamiliarFauna-1.12.2-1.0.11.jar) |
| [Forestry](https://github.com/ForestryMC/ForestryMC) | [forestry](https://curseforge.com/minecraft/mc-mods/forestry) | [5.8.2.422](https://mediafiles.forgecdn.net/files/2918/418/forestry_1.12.2-5.8.2.422.jar) |
| [Galacticraft](https://micdoodle8.com/mods/galacticraft) | minecraft-galacticraft-mod | [4.0.2.280](https://micdoodle8.com/new-builds/GC-1.12/280/GalacticraftCore-1.12.2-4.0.2.280.jar) |
| [Galacticraft Planets](https://github.com/SteveKunG/MorePlanets) | galacticraft-i-cant-find-it | [4.0.2.280](https://micdoodle8.com/new-builds/GC-1.12/280/Galacticraft-Planets-1.12.2-4.0.2.280.jar) |
| [Immersive Engineering](https://github.com/BluSunrize/ImmersiveEngineering) | [immersive-engineering](https://curseforge.com/minecraft/mc-mods/immersive-engineering) | [0.12-98](https://mediafiles.forgecdn.net/files/2974/106/ImmersiveEngineering-0.12-98.jar) |
| [Industrial Craft](http://wiki.industrial-craft.net) | [industrial-craft](https://curseforge.com/minecraft/mc-mods/industrial-craft) | [2.8.222-ex112](https://mediafiles.forgecdn.net/files/3838/713/industrialcraft-2-2.8.222-ex112.jar) |
| [Iron Chests](https://github.com/progwml6/ironchest) | [iron-chests](https://curseforge.com/minecraft/mc-mods/iron-chests) | [1.12.2-7.0.72.847](https://mediafiles.forgecdn.net/files/2747/935/ironchest-1.12.2-7.0.72.847.jar) |curse
| [Just Enough Items](https://github.com/mezz/JustEnoughItems) | [jei](https://curseforge.com/minecraft/mc-mods/jei) | [4.16.1.302](https://mediafiles.forgecdn.net/files/3043/174/jei_1.12.2-4.16.1.302.jar) |
| [LLibray](https://github.com/iLexiconn/LLibrary) | [llibrary](https://curseforge.com/minecraft/mc-mods/llibrary) | [1.7.20](https://mediafiles.forgecdn.net/files/3116/493/llibrary-1.7.20-1.12.2.jar) |
| [Mekanism Generators](https://curseforge.com/minecraft/mc-mods/mekanism-generators) | [mekanism-generators](https://curseforge.com/minecraft/mc-mods/mekanism-generators) | [1.12.2-9.8.3.390](https://mediafiles.forgecdn.net/files/2835/177/MekanismGenerators-1.12.2-9.8.3.390.jar) |
| [Mekanism](https://curseforge.com/minecraft/mc-mods/mekanism) | [mekanism](https://curseforge.com/minecraft/mc-mods/mekanism) | [1.12.2-9.8.3.390](https://mediafiles.forgecdn.net/files/2835/175/Mekanism-1.12.2-9.8.3.390.jar) |
| [MicdoodleCore](https://github.com/micdoodle8/MicdoodleCore) | project-501948 | [4.0.2.280](https://micdoodle8.com/new-builds/GC-1.12/280/MicdoodleCore-1.12.2-4.0.2.280.jar) |
| [Mowzie's Mobs](https://github.com/BobMowzie/MowziesMobs) | [mowzies-mobs](https://curseforge.com/minecraft/mc-mods/mowzies-mobs) | [1.5.8](https://mediafiles.forgecdn.net/files/3048/685/mowziesmobs-1.5.8.jar) |
| [MrCrayfish's Furniture Mod](https://mrcrayfish.com/mods?id=cfm) | [mrcrayfish-furniture-mod](https://curseforge.com/minecraft/mc-mods/mrcrayfish-furniture-mod) | [6.3.2](https://mediafiles.forgecdn.net/files/3865/259/furniture-6.3.2-1.12.2.jar) |
| [MrCrayfish's Gun Mod](https://mrcrayfish.com/mods?id=cgm) | [mrcrayfishs-gun-mod](https://curseforge.com/minecraft/mc-mods/mrcrayfishs-gun-mod) | [0.15.3](https://mediafiles.forgecdn.net/files/2854/400/guns-0.15.3-1.12.2.jar) |
| [MrCrayfish's Vehicle Mod](https://mrcrayfish.com/mods?id=vehicle) | [mrcrayfishs-vehicle-mod](https://www.curseforge.com/minecraft/mc-mods/mrcrayfishs-vehicle-mod) | [0.44.1](https://mediafiles.forgecdn.net/files/2967/287/vehicle-mod-0.44.1-1.12.2.jar) |
| [MrTJPCore](https://github.com/MrTJP/MrTJPCore) | [mrtjpcore](https://curseforge.com/minecraft/mc-mods/mrtjpcore) | [2.1.4.43](https://mediafiles.forgecdn.net/files/2735/197/MrTJPCore-1.12.2-2.1.4.43-universal.jar) |
| [Multi Mob Core](https://github.com/Daveyx0/MultiMob) | [multi-mob-core](https://curseforge.com/minecraft/mc-mods/multi-mob-core) | [1.0.5](https://mediafiles.forgecdn.net/files/2666/556/multimob-1.0.5.jar) |
| [NuclearCraft](https://github.com/turbodiesel4598/NuclearCraft) | [nuclearcraft-mod](https://curseforge.com/minecraft/mc-mods/nuclearcraft-mod) | [2.18zzz](https://mediafiles.forgecdn.net/files/3784/145/NuclearCraft-2.18zzz-1.12.2.jar) |
| [Obfuscate](https://mrcrayfish.com/mods?id=obfuscate) | [obfuscate](https://curseforge.com/minecraft/mc-mods/obfuscate) | [0.4.2](https://mediafiles.forgecdn.net/files/2916/310/obfuscate-0.4.2-1.12.2.jar) |
| [Open Terrain Generator](https://github.com/PG85/OpenTerrainGenerator) | [open-terrain-generator](https://www.curseforge.com/minecraft/mc-mods/open-terrain-generator) | [v9.4](https://mediafiles.forgecdn.net/files/3559/986/OpenTerrainGenerator-1.12.2-v9.4.jar) |
| [Pam's HarvestCraft](https://github.com/MatrexsVigil/harvestcraft) | [pams-harvestcraft](https://curseforge.com/minecraft/mc-mods/pams-harvestcraft) | [1.12.2zg](https://mediafiles.forgecdn.net/files/2904/825/Pam%27s+HarvestCraft+1.12.2zg.jar) |
| [ProjectE](https://curseforge.com/minecraft/mc-mods/projecte) | [projecte](https://curseforge.com/minecraft/mc-mods/projecte) | [1.12.2-PE1.4.1](https://mediafiles.forgecdn.net/files/2702/991/ProjectE-1.12.2-PE1.4.1.jar) |
| [Project Red Compat](https://github.com/MrTJP/ProjectRed) | [project-red-compat](https://curseforge.com/minecraft/mc-mods/project-red-compat) | [1.0.0](https://mediafiles.forgecdn.net/files/2745/546/ProjectRed-1.12.2-4.9.4.120-compat.jar) |
| [Project Red Expansion](https://github.com/MrTJP/ProjectRed) | [project-red-expansion](https://curseforge.com/minecraft/mc-mods/project-red-expansion) | [4.9.4.120](https://mediafiles.forgecdn.net/files/2745/550/ProjectRed-1.12.2-4.9.4.120-mechanical.jar) |
| [Project Red Exploration](https://github.com/MrTJP/ProjectRed) | [project-red-exploration](https://curseforge.com/minecraft/mc-mods/project-red-exploration) | [4.9.4.120](https://mediafiles.forgecdn.net/files/2745/551/ProjectRed-1.12.2-4.9.4.120-world.jar) |
| [Project Red Fabrication](https://github.com/MrTJP/ProjectRed) | [project-red-fabrication](https://curseforge.com/minecraft/mc-mods/project-red-fabrication) | [4.9.4.120](https://mediafiles.forgecdn.net/files/2745/547/ProjectRed-1.12.2-4.9.4.120-fabrication.jar) |
| [Project Red](https://github.com/MrTJP/ProjectRed) | [project-red-core](https://curseforge.com/minecraft/mc-mods/project-red-core) | [4.9.4.120](https://mediafiles.forgecdn.net/files/2745/545/ProjectRed-1.12.2-4.9.4.120-Base.jar) |
| [Project Red Illumination](https://github.com/MrTJP/ProjectRed) | [project-red-illumination](https://curseforge.com/minecraft/mc-mods/project-red-illumination) | [4.9.4.120](https://mediafiles.forgecdn.net/files/2745/549/ProjectRed-1.12.2-4.9.4.120-lighting.jar) |
| [Project Red Integration](https://github.com/MrTJP/ProjectRed) | [project-red-integration](https://curseforge.com/minecraft/mc-mods/project-red-integration) | [4.9.4.120](https://mediafiles.forgecdn.net/files/2745/548/ProjectRed-1.12.2-4.9.4.120-integration.jar) |
| [Refined Storage](https://curseforge.com/minecraft/mc-mods/refined-storage) | [refined-storage](https://curseforge.com/minecraft/mc-mods/refined-storage) | [1.6.16](https://mediafiles.forgecdn.net/files/2940/914/refinedstorage-1.6.16.jar) |
| [Traverse Reforged](https://github.com/MysticMods/Traverse) | [traverse-reforged](https://curseforge.com/minecraft/mc-mods/traverse-reforged) | [1.6.0-69](https://mediafiles.forgecdn.net/files/2613/657/Traverse-1.12.2-1.6.0-69.jar) |
| [TreeCapitator](https://github.com/mrihtar/Treecapitator) | [treecapitator-updated](https://curseforge.com/minecraft/mc-mods/treecapitator-updated) | [1.43.0](https://mediafiles.forgecdn.net/files/2722/878/%5b1.12%5dTreeCapitator-client-1.43.0.jar) |

download the mods separately if you want to:
(also configure Beta Days to show old HUD if you don't use the modpack)

### performance

some client-side mods from [this list](https://github.com/NordicGamerFE/usefulmods/blob/main/Performance/Performance112.md)

| mod  | slug | version |
| --- | --- | --- |
| [Better Biome Blend](https://github.com/FionaTheMortal/Better-Biome-Blend) | [better-biome-blend](https://curseforge.com/minecraft/mc-mods/better-biome-blend) | [1.1.7](https://mediafiles.forgecdn.net/files/3554/486/betterbiomeblend-1.12.2-1.1.7-forge.jar) |
| [BetterFPS](https://github.com/Guichaguri/BetterFps) | [betterfps](https://curseforge.com/minecraft/mc-mods/betterfps) | [1.4.8](https://mediafiles.forgecdn.net/files/2483/393/BetterFps-1.4.8.jar) |
| [Clumps](https://github.com/jaredlll08/Clumps) | [clumps](https://curseforge.com/minecraft/mc-mods/clumps) | [3.1.2](https://mediafiles.forgecdn.net/files/2666/198/Clumps-3.1.2.jar) |
| [FoamFix](https://github.com/asiekierka/FoamFix) | [foamfix-optimization-mod](https://curseforge.com/minecraft/mc-mods/foamfix-optimization-mod) | [0.10.14](https://mediafiles.forgecdn.net/files/3327/893/foamfix-0.10.14-1.12.2.jar) |
| [Phosphor](https://github.com/jellysquid3/phosphor-forge) | [phosphor-forge](https://curseforge.com/minecraft/mc-mods/phosphor-forge) | [0.2.7](https://mediafiles.forgecdn.net/files/2919/497/phosphor-forge-mc1.12.2-0.2.7-universal.jar) |
| TexFix | [texfix](https://curseforge.com/minecraft/mc-mods/texfix) | [4.0](https://mediafiles.forgecdn.net/files/2518/68/TexFix+V-1.12-4.0.jar) |
| [VanillaFix](https://github.com/DimensionalDevelopment/VanillaFix) | [vanillafix](https://curseforge.com/minecraft/mc-mods/vanillafix) | [1.0.10-150](https://mediafiles.forgecdn.net/files/2915/154/VanillaFix-1.0.10-150.jar) |

### recommended

minimal + performance + our personal picks

| mod  | slug | version |
| --- | --- | --- |
| [AppleSkin](https://github.com/squeek502/AppleSkin) | [appleskin](https://curseforge.com/minecraft/mc-mods/appleskin) | [1.0.14](https://mediafiles.forgecdn.net/files/2987/247/AppleSkin-mc1.12-1.0.14.jar) |
| Biome Colorizer | [biome-colorizer](https://curseforge.com/minecraft/mc-mods/biome-colorizer) | [1.0.2](https://mediafiles.forgecdn.net/files/3057/193/BiomeColorizer-1.12.2-v1.0.2.jar) |
| [Durability Notifier](https://github.com/Mrbysco/durability-notifier) | [durability-notifier](https://curseforge.com/minecraft/mc-mods/durability-notifier) | [1.0.0.1](https://cdn-raw.modrinth.com/data/yGpT1GvE/versions/1.0.0.1/Durability%2BNotifier-1.0.0.jar) |
| [Inventory Tweaks](https://curseforge.com/minecraft/mc-mods/inventory-tweaks) | [inventory-tweaks](https://curseforge.com/minecraft/mc-mods/inventory-tweaks) | [1.64+dev151](https://mediafiles.forgecdn.net/files/2923/460/InventoryTweaks-1.64%2Bdev.151.jar) |
| [Modern Splash](https://modrinth.com/mod/modern-splash) | [modern-splash](https://curseforge.com/minecraft/mc-mods/modern-splash) | [1.1.2](https://cdn-raw.modrinth.com/data/WQC4Wtzw/versions/1.1.2/modernsplash-1.12.2-1.1.2.jar) |
| [Traveler's Dream](https://github.com/Darsenia/Traveler-s-Dream) | [travelers-dream](https://curseforge.com/minecraft/mc-mods/travelers-dream) | [1.2](https://mediafiles.forgecdn.net/files/2980/60/Traveler%27sDream-Compact-1.12.2-v1.2.jar) |
| [WIT (What Is That)](https://github.com/SilentChaos512/WIT) | [wit-what-is-that](https://curseforge.com/minecraft/mc-mods/wit-what-is-that) | [1.0.25-44](https://mediafiles.forgecdn.net/files/2448/921/WIT-1.12-1.0.25-44.jar) |
| [Xaero's Minimap](https://chocolateminecraft.com/minimap2.php) | [xaeros-minimap](https://curseforge.com/minecraft/mc-mods/xaeros-minimap) | [22.13.0](https://chocolateminecraft.com/mods2/minimap/Xaeros_Minimap_22.13.0_Forge_1.12.jar) |
| [Xaero's World Map](https://chocolateminecraft.com/worldmap.php) | [xaeros-world-map](https://curseforge.com/minecraft/mc-mods/xaeros-world-map) | [1.26.2](https://chocolateminecraft.com/mods2/worldmap/XaerosWorldMap_1.26.2_Forge_1.12.jar) |

1.0.5
